<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/7/4
 * Time: 下午 05:35
 */

namespace Drupal\mixin;

use Drupal\mixin\Traits\Object;

class Setup {
  use Object {
    create as protected;
  };

  static function hookExit($destination = NULL) {
    static::create()->doExit();
  }

  protected function doExit() {
  }

  static function hookBoot() {
    static::create()->doBoot();
  }

  protected function doBoot() {
  }

  static function hookInit() {
    static::create()->doInit();
  }

  protected function doInit() {
  }

  static function hookInstall() {
    static::create()->doInstall();
  }

  protected function doInstall() {
  }
}
