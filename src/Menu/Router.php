<?php

namespace Drupal\mixin\Menu;

use Drupal\mixin\Filter;
use Drupal\mixin\SettingExport;

class Router extends SettingExport {

  //<editor-fold desc="Properties">
  /** @var string */
  protected $title;
  /** @var callback */
  protected $title_callback;
  /** @var array */
  protected $title_arguments;
  /** @var string */
  protected $description;
  /** @var callback */
  protected $page_callback;
  /** @var array */
  protected $page_arguments;
  /** @var callback */
  protected $delivery_callback;
  /** @var callback */
  protected $access_callback;
  /** @var array */
  protected $access_arguments;
  /** @var callback */
  protected $theme_callback;
  /** @var array */
  protected $theme_arguments;
  /** @var string */
  protected $file;
  /** @var string */
  protected $file_path;
  /** @var array */
  protected $load_arguments;
  /** @var integer */
  protected $weight;
  /** @var string */
  protected $menu_name;
  /** @var boolean */
  protected $expanded;
  /** @var integer */
  protected $context;
  /** @var string */
  protected $tab_parent;
  /** @var string */
  protected $tab_root;
  /** @var string */
  protected $position;
  /** @var integer */
  protected $type;
  /** @var array */
  protected $options;

  //</editor-fold>

  function setTitle($value = NULL) {
    $this->title = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setTitleCallback($value = NULL) {
    $this->title_callback = Filter::sanitizeCallbackFunction($value, TRUE);
    return $this;
  }

  function setTitleArguments(array $value = NULL) {
    $this->title_arguments = isset($value) ? $value : NULL;
    return $this;
  }

  function setDescription($value = NULL) {
    $this->description = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setPageCallback($value = NULL) {
    $this->page_callback = Filter::sanitizeCallbackFunction($value, TRUE);
    return $this;
  }

  function setPageArguments(array $value = NULL) {
    $this->page_arguments = isset($value) ? $value : NULL;
    return $this;
  }

  function setDeliveryCallback($value = NULL) {
    $this->delivery_callback = Filter::sanitizeCallbackFunction($value, TRUE);
    return $this;
  }

  function setAccessCallback($value = NULL) {
    $this->access_callback = is_bool($value) ? $value : Filter::sanitizeCallbackFunction($value, TRUE);
    return $this;
  }

  function setAccessArguments(array $value = NULL) {
    $this->access_arguments = isset($value) ? $value : NULL;
    return $this;
  }

  function setThemeCallback($value = NULL) {
    $this->theme_callback = $this->title_callback = Filter::sanitizeCallbackFunction($value, TRUE);
    return $this;
  }

  function setThemeArguments(array $value = NULL) {
    $this->theme_arguments = isset($value) ? $value : NULL;
    return $this;
  }

  function setFile($value = NULL) {
    $this->file = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setFilePath($value = NULL) {
    $this->file_path = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setLoadArguments(array $value = NULL) {
    $this->load_arguments = isset($value) ? $value : NULL;
    return $this;
  }

  function setWeight($value = NULL) {
    $this->weight = Filter::sanitize(Filter::TYPE_INTEGER, $value, TRUE);
    return $this;
  }

  function setMenuName($value = NULL) {
    $this->menu_name = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setExpanded($value = NULL) {
    $this->expanded = Filter::sanitize(Filter::TYPE_BOOLEAN, $value, TRUE);
    return $this;
  }

  function setContext($value = NULL) {
    $this->context = Filter::sanitize(Filter::TYPE_INTEGER, $value, TRUE);
    return $this;
  }

  function setTabParent($value = NULL) {
    $this->tab_parent = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setTabRoot($value = NULL) {
    $this->tab_root = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setPosition($value = NULL) {
    $this->position = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setType($value = NULL) {
    $this->type = Filter::sanitize(Filter::TYPE_INTEGER, $value, TRUE);
    return $this;
  }

  function setOptions(array $value = NULL) {
    $this->options = isset($value) ? $value : NULL;
    return $this;
  }

  protected function exportMapping() {
    $mapping = [
      'title_callback' => 'title callback',
      'title_arguments' => 'title arguments',
      'page_callback' => 'page callback',
      'page_arguments' => 'page arguments',
      'delivery_callback' => 'delivery callback',
      'access_callback' => 'access callback',
      'access_arguments' => 'access arguments',
      'theme_callback' => 'theme callback',
      'theme_arguments' => 'theme arguments',
      'file_path' => 'file path',
      'load_arguments' => 'load arguments',
    ];

    return $mapping;
  }
}
