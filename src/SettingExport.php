<?php
namespace Drupal\mixin;

use Drupal\mixin\Traits\PropertyBag;

abstract class SettingExport {
  use PropertyBag;

  static function create(array $data = []) {
    return new static($data);
  }

  function export($key = NULL) {
    return ('' == static::exportMethodPattern()) ? $this->exportProperties($key) : $this->exportMethods();
  }

  protected static function exportMethodPattern() {
    return '';
  }

  function exportProperties($key = NULL) {
    $items = [];
    $mapping = $this->exportMapping();

    foreach (get_object_vars($this) as $index => $item) {
      if (FALSE == isset($item)) {
        continue;
      }

      $builder = function () use ($item) {
        if (FALSE == is_object($item)) {
          return $item;
        } else if ($item instanceof self) {
          return $item->exportProperties();
        } elseif ($item instanceof Enumeration) {
          return $item->getValue();
        } else {
          return $item;
        }
      };

      $index = isset($mapping[$index]) ? $mapping[$index] : $index;
      Getter::create($items, [$index])->setBuilder($builder)->fetch();
    }

    return isset($key) ? [$key => $items] : $items;
  }

  protected function exportMapping() {
    return [];
  }

  function exportMethods() {
    $data = [];

    foreach (static::exportMethodNames() as $name => $isStatic) {
      if (FALSE == $isStatic) {
        $data += $this->$name();
      }
    }

    return $data;
  }

  protected static function exportMethodNames() {
    static $cache = [];

    $builder = function () {
      return static::filterMethodNames(static::exportMethodPattern());
    };

    $items = Getter::create($cache, [get_called_class()])->setBuilder($builder)->fetch();

    return $items;
  }

}
