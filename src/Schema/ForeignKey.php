<?php

namespace Drupal\mixin\Schema;

use Drupal\mixin\Filter;
use Drupal\mixin\SettingExport;

class ForeignKey extends SettingExport {

  protected $table = '';
  protected $columns = [];

  function setTable($value) {
    $this->table = Filter::sanitize(Filter::TYPE_STRING, $value);
    return $this;
  }

  function setColumns(array $value) {
    $this->columns = $value;
    return $this;
  }

}
