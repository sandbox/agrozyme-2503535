<?php
namespace Drupal\mixin\Schema;

use Drupal\mixin\Filter;
use Drupal\mixin\SettingExport;

class Field extends SettingExport {

  //<editor-fold desc="Properties">
  /** @var string */
  protected $description;
  /** @var Type */
  protected $type;
  /** @var Size */
  protected $size;
  /** @var boolean */
  protected $not_null;
  /** @var mixed */
  protected $default;
  /** @var integer */
  protected $length;
  /** @var boolean */
  protected $unsigned;
  /** @var integer */
  protected $precision;
  /** @var integer */
  protected $scale;
  /** @var boolean */
  protected $serialize;

  //</editor-fold>

  function setDescription($value = NULL) {
    $this->description = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setLength($value = NULL) {
    $this->length = Filter::sanitize(Filter::TYPE_INTEGER, $value, TRUE);
    return $this;
  }

  function setNotNull($value = NULL) {
    $this->not_null = Filter::sanitize(Filter::TYPE_BOOLEAN, $value, TRUE);
    return $this;
  }

  function setPrecision($value = NULL) {
    $this->precision = Filter::sanitize(Filter::TYPE_INTEGER, $value, TRUE);
    return $this;
  }

  function setScale($value = NULL) {
    $this->scale = Filter::sanitize(Filter::TYPE_INTEGER, $value, TRUE);
    return $this;
  }

  function setSerialize($value = NULL) {
    $this->serialize = Filter::sanitize(Filter::TYPE_BOOLEAN, $value, TRUE);
    return $this;
  }

  function setSize(Size $value = NULL) {
    $this->size = $value;
    return $this;
  }

  function setType(Type $value = NULL, $default = NULL, $extra = []) {
    $this->type = $value;
    $this->setDefault($default);

    if (isset($value)) {
      foreach ($extra as $index => $item) {
        $this->{$index . '_type'} = $item;
      }
    }

    return $this;
  }

  protected function setDefault($value = NULL) {
    if ((FALSE == isset($value)) || (FALSE == isset($this->type))) {
      $this->default = NULL;
      return $this;
    }

    switch ($this->type->getValue()) {
      case 'varchar':
      case 'char':
      case 'text':
      case 'numeric':
      case 'datetime':
        $this->default = strval($value);
      break;

      case 'int':
      case 'serial':
        $this->default = intval($value);
      break;

      case 'float':
        $this->default = floatval($value);
      break;

      default:
        $this->default = $value;
    }

    return $this;
  }

  function setUnsigned($value = NULL) {
    $this->unsigned = Filter::sanitize(Filter::TYPE_BOOLEAN, $value, TRUE);
    return $this;
  }

  protected function exportMapping() {
    $mapping = ['not_null' => 'not null',];
    return $mapping;
  }

}
