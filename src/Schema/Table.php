<?php
namespace Drupal\mixin\Schema;

use Drupal\mixin\Filter;
use Drupal\mixin\SettingExport;

class Table extends SettingExport {

  //<editor-fold desc="Properties">
  /** @var string */
  protected $description;
  /** @var Field[] */
  protected $fields;
  /** @var array */
  protected $indexes;
  /** @var array */
  protected $unique_keys;
  /** @var array */
  protected $primary_key;
  /** @var ForeignKey[] */
  protected $foreign_keys;

  //</editor-fold>

  function setDescription($value = NULL) {
    $this->description = Filter::sanitize(Filter::TYPE_STRING, $value, TRUE);
    return $this;
  }

  function setFields(array $value = NULL) {
    $this->fields = Filter::sanitizeArray(Field::className(), $value, TRUE);
    return $this;
  }

  function setIndexes(array $value = NULL) {
    $this->indexes = Filter::sanitize(Filter::TYPE_ARRAY, $value, TRUE);
    return $this;
  }

  function setUniqueKeys(array $value = NULL) {
    $this->unique_keys = Filter::sanitize(Filter::TYPE_ARRAY, $value, TRUE);
    return $this;
  }

  function setPrimaryKey(array $value = NULL) {
    $this->primary_key = Filter::sanitize(Filter::TYPE_ARRAY, $value, TRUE);
    return $this;
  }

  function setForeignKeys(array $value = NULL) {
    $this->foreign_keys = Filter::sanitizeArray(ForeignKey::className(), $value, TRUE);
    return $this;
  }

  protected function exportMapping() {
    $mapping = ['unique_keys' => 'unique keys', 'primary_key' => 'primary key', 'foreign_keys' => 'foreign keys'];
    return $mapping;
  }

}
