<?php
namespace Drupal\mixin\Schema;

use Drupal\mixin\Enumeration;

class Size extends Enumeration {

  static function tiny() {
    return static::getItem(__FUNCTION__);
  }

  protected static function getItem($index) {
    return static::getObject($index, $index);
  }

  protected static function setupMethodPattern() {
    return ['tiny', 'small', 'normal', 'medium', 'big'];
  }

  static function small() {
    return static::getItem(__FUNCTION__);
  }

  static function normal() {
    return static::getItem(__FUNCTION__);
  }

  static function medium() {
    return static::getItem(__FUNCTION__);
  }

  static function big() {
    return static::getItem(__FUNCTION__);
  }

}
