<?php

namespace Drupal\mixin\Schema;

use Drupal\mixin\Enumeration;

class Type extends Enumeration {
  static function varchar() {
    return static::getItem(__FUNCTION__);
  }

  protected static function getItem($index) {
    return static::getObject($index, $index);
  }

  protected static function setupMethodPattern() {
    $items = ['varchar', 'char', 'int', 'serial', 'float', 'numeric', 'text', 'blob', 'datetime'];
    return $items;
  }

  static function char() {
    return static::getItem(__FUNCTION__);
  }

  static function int() {
    return static::getItem(__FUNCTION__);
  }

  static function serial() {
    return static::getItem(__FUNCTION__);
  }

  static function float() {
    return static::getItem(__FUNCTION__);
  }

  static function numeric() {
    return static::getItem(__FUNCTION__);
  }

  static function text() {
    return static::getItem(__FUNCTION__);
  }

  static function blob() {
    return static::getItem(__FUNCTION__);
  }

  static function datetime() {
    return static::getItem(__FUNCTION__);
  }
}
