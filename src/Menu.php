<?php
namespace Drupal\mixin;

abstract class Menu extends SettingExport {

  protected static function exportMethodPattern() {
    return '/^menu[A-Z]/';
  }
}
