<?php
namespace Drupal\mixin;

use Drupal\mixin\Parameter\ParserList;
use Drupal\mixin\Traits\Object;

abstract class Drush extends SettingExport {
  use Object;

  function export($key = NULL) {
    $class = get_called_class();
    $module = static::getType()->getModule();

    foreach ($this->doMapping() as $name => $method) {
      $function = 'drush_' . $module . '_' . $name;
      $wrapper = ParserList::create([$class, $method]);
      $parameter = $wrapper->toParameterListString();
      $argument = $wrapper->toArgumentListString();
      Caller::createFunction($function, $parameter, "return {$class}::create()->{$method}({$argument});");
    }

    return parent::export($key);
  }

  protected function doMapping() {
    return [];
  }

  protected static function exportMethodPattern() {
    return '/^command[A-Z]/';
  }
}
