<?php

namespace Drupal\mixin\Traits;

use Drupal\mixin\Getter;

trait Singleton {
  use Object {
    create as protected;
  }

  protected function __construct() {
  }

  /** @return self */
  static function getObject() {
    static $cache = [];

    $bulider = function () {
      return new static();
    };

    $item = Getter::create($cache, [get_called_class()])->setBuilder($bulider)->fetch();

    return $item;
  }
}
