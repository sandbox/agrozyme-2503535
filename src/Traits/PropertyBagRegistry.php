<?php

namespace Drupal\mixin\Traits;

use Drupal\mixin\Caller;
use Drupal\mixin\Getter;

trait PropertyBagRegistry {
  use PropertyBag;

  /** @return self|null */
  static function getItem($index) {
    $items = static::getAll();
    return Getter::create($items, [$index])->fetch(FALSE);
  }

  static function getAll() {
    static $cache = [];
    $class = get_called_class();

    $builder = function () use ($class) {
      $items = [];

      foreach (static::setupMethodNames() as $name => $isStatic) {
        if (FALSE == $isStatic) {
          continue;
        }

        $index = static::formatIndex($name);
        $item = Caller::invoke([$class, $name]);

        if (($item instanceof $class) && ('' !== $index)) {
          $items[$index] = $item;
        }
      }

      return $items;
    };

    $items = Getter::create($cache, [$class])->setBuilder($builder)->fetch();
    return $items;
  }

  static function formatIndex($name) {
    return $name;
  }
}
