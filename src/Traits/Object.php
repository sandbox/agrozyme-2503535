<?php

namespace Drupal\mixin\Traits;

use Drupal\mixin\Type;

trait Object {

  static function className() {
    return get_called_class();
  }

  static function getType() {
    return Type::create(get_called_class(), FALSE);
  }

  static function create() {
    return new static();
  }

  function toArray() {
    return get_object_vars($this);
  }

}
