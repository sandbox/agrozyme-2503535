<?php

namespace Drupal\mixin\Traits;

use Drupal\mixin\Getter;
use Drupal\mixin\Type;

trait Factory {
  use Object;

  static function getObject($class, array $arguments = [], $exceptionHandle = NULL) {
    $module = reset($arguments);
    $item = Type::create(static::getClass($module, $class))->getObject($arguments, $exceptionHandle);
    return $item;
  }

  static function getClass($module, $class) {
    static $cache = [];

    $builder = function () use ($module, $class) {
      $filter = function ($item) {
        return class_exists($item);
      };

      $classes = array_filter(static::guessClasses($module, $class), $filter);
      return empty($classes) ? '' : reset($classes);
    };

    $writeBack = function ($item) {
      return ('' !== $item);
    };

    $item = Getter::create($cache, [$module, $class])->setBuilder($builder)->fetch($writeBack);

    return $item;
  }

  protected static function guessClasses($module, $class) {
    $parse = static::getType();
    $items = [];

    if ($module) {
      $items[] = Type::getDrupalClassName($module, $class);
    }

    $names = [$parse->getVendor(), $parse->getModule(), $class];
    $items[] = implode('\\', $names);
    return $items;
  }
}
