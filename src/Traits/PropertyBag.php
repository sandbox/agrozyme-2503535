<?php

namespace Drupal\mixin\Traits;

use Drupal\mixin\Caller;
use Drupal\mixin\Getter;
use ReflectionMethod;

trait PropertyBag {
  use Object;

  function __construct(array $data = []) {
    $this->setup();
    $this->merge($data);
  }

  protected function setup() {
    foreach (static::setupMethodNames() as $name => $isStatic) {
      if (FALSE == $isStatic) {
        Caller::invoke([$this, $name]);
      }
    }
  }

  protected static function setupMethodNames() {
    static $cache = [];

    $builder = function () {
      return static::filterMethodNames(static::setupMethodPattern());
    };

    $items = Getter::create($cache, [get_called_class()])->setBuilder($builder)->fetch();

    return $items;
  }

  protected static function filterMethodNames($pattern) {
    $mapping = function (ReflectionMethod $method) {
      return $method->isStatic();
    };

    $items = static::getType()->getReflectionMethods(static::makeMethodFilter($pattern));
    return array_map($mapping, $items);
  }

  protected static function makeMethodFilter($pattern) {
    /** @var Caller $wrapper */
    $wrapper = Caller::create()->setDefault(FALSE);

    if (FALSE === isset($pattern)) {
      $wrapper->setDefault(TRUE);
    } else if (is_array($pattern)) {
      $pattern = array_combine($pattern, $pattern);

      $wrapper->setCallback(function ($name) use ($pattern) {
        return isset($pattern[$name]);
      });
    } else if (is_string($pattern) && ('' !== $pattern)) {
      $wrapper->setCallback(function ($name) use ($pattern) {
        return preg_match($pattern, $name);
      });
    } else {
      $wrapper->setDefault(FALSE);
    }

    $filter = function (ReflectionMethod $method) use ($wrapper) {
      if ((0 === $method->getNumberOfParameters())) {
        return $wrapper($method->name);
      } else {
        return FALSE;
      }
    };

    return $filter;
  }

  /** @return array|string|boolean */
  protected static function setupMethodPattern() {
    return [];
  }

  function merge(array $data = []) {
    $default = get_object_vars($this);

    foreach ($data as $name => $value) {
      if (FALSE === array_key_exists($name, $default)) {
        continue;
      }

      if (is_array($value) && is_array($default[$name])) {
        $value += $default[$name];
      }

      $this->$name = $value;
    }
  }
}
