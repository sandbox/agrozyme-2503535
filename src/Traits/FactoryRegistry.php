<?php

namespace Drupal\mixin\Traits;

use Drupal\mixin\Getter;
use Drupal\mixin\Type;

trait FactoryRegistry {
  use Factory {
    getObject as protected getObjectCall;
  }

  static function getObject($class, array $arguments = [], $exceptionHandle = NULL) {
    static $cache = [];
    $index = Type::getDrupalClassName(reset($arguments), $class);

    $builder = function () use ($class, $arguments, $exceptionHandle) {
      return static::getObjectCall($class, $arguments, $exceptionHandle);
    };

    $writeBack = function ($item) {
      return is_object($item);
    };

    $item = Getter::create($cache, [$index])->setBuilder($builder)->fetch($writeBack);
    return $item;
  }
}
