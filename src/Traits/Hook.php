<?php
namespace Drupal\mixin\Traits;

use Drupal\mixin\Arrays;
use Drupal\mixin\Caller;
use Drupal\mixin\Parameter\ParserList;
use ReflectionMethod;

trait Hook {
  use Object;

  static function createHooks() {
    $callback = function ($method, $function, $class) {
      $wrapper = ParserList::create([$class, $method]);
      $parameter = $wrapper->toParameterListString();
      $argument = $wrapper->toArgumentListString();
      Caller::createFunction($function, $parameter, "return {$class}::{$method}({$argument});");
    };

    foreach (static::filterHookMap() as $class => $items) {
      Arrays::walk($items, $callback, $class);
    }
  }

  protected static function filterHookMap() {
    $data = [];
    $map = static::getHookMap();

    if (FALSE == is_array($map)) {
      return $data;
    }

    $filter = function ($method, $function, $class) {
      $callback = [$class, $method];
      return ((FALSE == function_exists($function)) &&
        is_callable($callback) &&
        (new ReflectionMethod($class, $method))->isStatic());
    };

    foreach ($map as $class => $items) {
      if (class_exists($class) && is_array($items) && (FALSE == empty($items))) {
        $data[$class] = Arrays::filter($items, $filter, $class);
      }
    }

    return $data;
  }

  protected static function getHookMap() {
    return [];
  }

}
