<?php

namespace Drupal\mixin\Decimal;

class BinaryCalculator extends Base {

  function add($value) {
    return $this->operate('bcadd', $value);
  }

  function compare($value) {
    return bccomp($this->value, static::create($value)->getValue(), static::scale());
  }

  function divide($value) {
    return $this->operate('bcdiv', $value);
  }

  function modulo($value) {
    return $this->operate('bcmod', $value);
  }

  function multiply($value) {
    return $this->operate('bcmul', $value);
  }

  function power($value) {
    return $this->operate('bcpow', $value);
  }

  function squareRoot() {
    return static::create(bcsqrt($this->value, static::scale()));
  }

  function subtract($value) {
    return $this->operate('bcsub', $value);
  }
}
