<?php

namespace Drupal\mixin\Decimal;

use Drupal\mixin\Getter;
use Drupal\mixin\Traits\Object;
use Exception;

abstract class Base {
  use Object;
  const DEFAULT_SCALE = 14;

  protected $value = '';

  protected function __construct($value) {
    $this->value = $value;
  }

  /** @return self */
  abstract function add($value);

  /** @return self */
  abstract function divide($value);

  /** @return self */
  abstract function modulo($value);

  /** @return self */
  abstract function multiply($value);

  /** @return self */
  abstract function power($value);

  /** @return self */
  abstract function squareRoot();

  /** @return self */
  abstract function subtract($value);

  function compareWith($operator, $value) {
    $callback = static::getOperatorCallback($operator);
    return is_callable($callback) ? call_user_func($callback, $this->compare($value)) : FALSE;
  }

  protected static function getOperatorCallback($operator) {
    static $cache;

    if (FALSE == isset($cache)) {
      $cache['<'] = function ($item) {
        return (-1 == $item);
      };

      $cache['=='] = function ($item) {
        return (0 == $item);
      };

      $cache['>'] = function ($item) {
        return (1 == $item);
      };

      $cache['<='] = function ($item) {
        return (0 >= $item);
      };

      $cache['!='] = function ($item) {
        return (0 != $item);
      };

      $cache['>='] = function ($item) {
        return (0 <= $item);
      };
    }

    return Getter::create($cache, [$operator])->fetch();
  }

  /** @return int */
  abstract function compare($value);

  function getValueString($scale = NULL) {
    $value = $this->__toString();

    if (is_int($scale)) {
      $scale = intval($scale);
      $offset = strpos($value, '.');

      if ((0 <= $scale) && (FALSE !== $offset)) {
        $offset += $scale + 1;
        $value = substr($value, 0, $offset);
      }
    }

    $value = trim($value, '0');
    $value = rtrim($value, '.');

    if (('' == $value) || ('.' == substr($value, 0, 1))) {
      $value = '0' . $value;
    }

    return $value;
  }

  function __toString() {
    return $this->value;
  }

  protected function operate($callback, $value) {
    if (FALSE == is_callable($callback)) {
      return new static('');
    }

    try {
      $arguments = [$this->value, static::create($value)->getValue(), static::scale()];
      $item = call_user_func_array($callback, $arguments);
      return static::create($item);
    } catch (Exception $exception) {
      return new static('');
    }
  }

  function getValue() {
    return $this->value;
  }

  static function create($value, $scale = NULL) {
    static::scale($scale);

    if ($value instanceof static) {
      return $value;
    }

    $value = strval($value);

    if (FALSE == is_numeric($value)) {
      $value = '';
    }

    return new static($value);
  }

  static function scale($scale = NULL) {
    static $cache;

    if ((FALSE == isset($cache)) || (is_bool($scale) && $scale)) {
      $cache = static::DEFAULT_SCALE;
    } else if (is_int($scale)) {
      $scale = intval($scale);

      if (0 <= $scale) {
        $cache = $scale;
      }
    }

    return $cache;
  }

}
