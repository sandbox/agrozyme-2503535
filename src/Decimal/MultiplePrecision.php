<?php
namespace Drupal\mixin\Decimal;

class MultiplePrecision extends Base {
  protected function __construct($value) {
    parent::__construct(gmp_init($value, 10));
  }

  function __toString() {
    return gmp_strval($this->value);
  }

  function add($value) {
    return $this->operate('gmp_add', $value);
  }

  function compare($value) {
    return gmp_cmp($this->value, static::create($value)->getValue());
  }

  static function create($value, $scale = NULL) {
    if (is_resource($value) && ('GMP integer' == get_resource_type($value))) {
      $value = gmp_strval($value);
    }

    return forward_static_call(__FUNCTION__, $value, $scale);
  }

  function divide($value) {
    //$scale = static::scale();
    //list($quotient, $remainder) = gmp_div_qr($this->value, static::create($value)->getValue());
    list($quotient) = gmp_div_qr($this->value, static::create($value)->getValue());
    return static::create($quotient);
  }

  function modulo($value) {
    return $this->operate('gmp_mod', $value);
  }

  function multiply($value) {
    return $this->operate('gmp_mul', $value);
  }

  function power($value) {
    return static::create(gmp_pow($this->value, intval($value)));
  }

  function squareRoot() {
    return static::create(gmp_sqrt($this->value));
  }

  function subtract($value) {
    return $this->operate('gmp_sub', $value);
  }

}
