<?php
namespace Drupal\mixin;

use Drupal\mixin\Traits\Object;
use Drupal\xautoload\ClassFinder\InjectedApi\FindFileInjectedApi;
use Exception;
use ReflectionClass;
use ReflectionMethod;

class Type {
  use Object;
  const SEPARATOR = '\\';

  protected $vendor = '';
  protected $module = '';
  protected $path = '';
  protected $name = '';
  protected $class = '';

  protected function __construct($class) {
    $names = explode(static::SEPARATOR, $class);
    $this->name = '' . array_pop($names);
    $this->vendor = '' . array_shift($names);
    $this->module = '' . array_shift($names);
    $this->path = implode(static::SEPARATOR, $names);
    $this->class = $class;
  }

  static function getDrupalClassName($module, $class) {
    return static::normalize(['Drupal', $module, $class]);
  }

  static function normalize($class) {
    if (is_array($class)) {
      $class = implode(static::SEPARATOR, $class);
    }

    static $pattern = '/[^a-zA-Z0-9_\x5c\x7f-\xff]/';
    $class = preg_replace($pattern, '', strval($class));
    $names = explode(static::SEPARATOR, $class);
    $names = Filter::sanitizeNames($names, FALSE);
    $class = implode(static::SEPARATOR, $names);
    return $class;
  }

  /** @return self */
  static function create($class, $strict = TRUE) {
    static $cache = [];
    $class = $strict ? static::normalize($class) : strval($class);

    $callback = function () use ($class) {
      return new static($class);
    };

    $item = Getter::create($cache, [$class])->setBuilder($callback)->fetch();
    return $item;
  }

  function getClass() {
    return $this->class;
  }

  function getModule() {
    return $this->module;
  }

  function getName() {
    return $this->name;
  }

  function getPath() {
    return $this->path;
  }

  function getVendor() {
    return $this->vendor;
  }

  function getClassFile() {
    $class = $this->class;
    $api = new FindFileInjectedApi($class);
    $file = (xautoload_get_finder()->apiFindFile($api, $class)) ? $api->getFile() : '';
    return $file;
  }

  /** @return ReflectionMethod[] */
  function getReflectionMethods($filter = NULL) {
    $items = [];
    $methods = $this->getReflectionClass()->getMethods();
    $callback = $this->buildMethodFilter($filter);

    /** @var ReflectionMethod $item */
    foreach (array_filter($methods, $callback) as $item) {
      $items[$item->name] = $item;
    }

    return $items;
  }

  function getReflectionClass() {
    return new ReflectionClass($this->class);
  }

  protected function buildMethodFilter($filter = NULL) {
    if (FALSE == isset($filter)) {
      $callback = function (ReflectionMethod $method) {
        return TRUE;
      };
    } else if (is_bool($filter)) {
      $callback = function (ReflectionMethod $method) use ($filter) {
        return $filter;
      };
    } else if (is_callable($filter)) {
      $callback = function (ReflectionMethod $method) use ($filter) {
        return Caller::invoke($filter, [$method], FALSE);
      };
    } else {
      $callback = function (ReflectionMethod $method) {
        return FALSE;
      };
    }

    return $callback;
  }

  function getObject(array $arguments = [], $exceptionHandle = NULL) {
    $class = $this->class;

    try {
      $reflex = new ReflectionClass($class);
      return $reflex->newInstanceArgs($arguments);
    } catch (Exception $exception) {
      if (is_callable($exceptionHandle)) {
        call_user_func($exceptionHandle, $exception, $class, $arguments);
      }

      return NULL;
    }
  }
}
