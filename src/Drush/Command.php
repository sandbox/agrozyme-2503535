<?php

namespace Drupal\mixin\Drush;

use Drupal\mixin\SettingExport;
use Drupal\mixin\Traits\PropertyBag;

class Command extends SettingExport {
  use PropertyBag;

  //<editor-fold desc="Properties">
  /** @var array $aliases */
  protected $aliases;
  /** @var string $command_hook */
  protected $command_hook;
  /** @var string $callback */
  protected $callback;
  /** @var array $callback_arguments */
  protected $callback_arguments;
  /** @var string $description */
  protected $description;
  /** @var array $arguments */
  protected $arguments;
  /** @var bool|integer $required_arguments */
  protected $required_arguments;
  /** @var array $options */
  protected $options;
  /** @var array $examples */
  protected $examples;
  /** @var string $scope */
  protected $scope;
  /** @var integer $bootstrap */
  protected $bootstrap;
  /** @var array $drupal_dependencies */
  protected $drupal_dependencies;

  //</editor-fold>

  public function setAliases(array $aliases) {
    $this->aliases = $aliases;
    return $this;
  }

  public function setCommandHook($command_hook) {
    $this->command_hook = '' . $command_hook;
    return $this;
  }

  public function setCallback($callback) {
    $this->callback = '' . $callback;
    return $this;
  }

  public function setCallbackArguments(array $callback_arguments) {
    $this->callback_arguments = $callback_arguments;
    return $this;
  }

  public function setDescription($description) {
    $this->description = '' . $description;
    return $this;
  }

  public function setArguments(array $arguments) {
    $this->arguments = $arguments;
    return $this;
  }

  public function setRequiredArguments($required_arguments) {
    $this->required_arguments = is_bool($required_arguments) ? $required_arguments : intval($required_arguments);
    return $this;
  }

  public function setOptions(array $options) {
    $this->options = $options;
    return $this;
  }

  public function setExamples(array $examples) {
    $this->examples = $examples;
    return $this;
  }

  public function setScope($scope) {
    $this->scope = '' . $scope;
    return $this;
  }

  public function setBootstrap($bootstrap) {
    $this->bootstrap = intval($bootstrap);
    return $this;
  }

  public function setDrupalDependencies(array $drupal_dependencies) {
    $this->drupal_dependencies = $drupal_dependencies;
    return $this;
  }

  protected function mapping() {
    $mapping = [
      'command_hook' => 'command-hook',
      'callback_arguments' => 'callback arguments',
      'required_arguments' => 'required-arguments',
      'drupal_dependencies' => 'drupal dependencies',
    ];

    return $mapping;
  }
}
