<?php

namespace Drupal\mixin;

use Drupal\mixin\Traits\Object;
use stdClass;

class Filter {
  use Object;

  const TYPE_ARRAY = 'array';
  const TYPE_BOOLEAN = 'boolean';
  const TYPE_FLOAT = 'float';
  const TYPE_INTEGER = 'integer';
  const TYPE_NULL = 'null';
  const TYPE_OBJECT = 'object';
  const TYPE_STRING = 'string';

  static function sanitizeArray($type, array $value = NULL, $nullable = FALSE) {
    if (FALSE == isset($value)) {
      return static::sanitize(static::TYPE_ARRAY, $value, $nullable);
    }

    if (is_callable($type)) {
      $callback = $type;
    } else if (class_exists($type)) {
      $callback = function ($index, $item) use ($type) {
        return $item instanceof $type;
      };
    } else {
      $callback = function ($index, $item) use ($type) {
        return (gettype($item) == $type);
      };
    }

    $value = Arrays::filter($value, $callback);
    return static::sanitize(static::TYPE_ARRAY, $value, $nullable);
  }

  static function sanitize($type, $value, $nullable = FALSE) {
    if (isset($value) && settype($value, $type)) {
      return $value;
    } else if ($nullable) {
      return NULL;
    } else {
      return static::getDefault($type);
    }
  }

  static function getDefault($type) {
    static $cache = [];

    if (empty($cache)) {
      $cache = [
        static::TYPE_ARRAY => [],
        static::TYPE_BOOLEAN => FALSE,
        static::TYPE_FLOAT => 0.0,
        static::TYPE_INTEGER => 0,
        static::TYPE_NULL => NULL,
        static::TYPE_OBJECT => new stdClass(),
        static::TYPE_STRING => '',
      ];
    }

    $default = &$cache[$type];
    return isset($default) ? $default : NULL;
  }

  static function sanitizeNames(array $names, $strict = TRUE) {
    static $pattern = '/[^a-zA-Z0-9_\x7f-\xff]/';

    if ($strict) {
      $map = function ($item) use ($pattern) {
        return preg_replace($pattern, '', strval($item));
      };

      $names = array_map($map, $names);
    }

    $filter = function ($item) {
      return ('' !== $item);
    };

    $names = array_filter($names, $filter);
    return $names;
  }

  static function sanitizeCallback($value, $nullable = FALSE) {
    if (isset($value) && is_callable($value)) {
      return $value;
    } else if ($nullable) {
      return NULL;
    } else {
      return '';
    }
  }

  static function sanitizeCallbackFunction($value, $nullable = FALSE) {
    if (isset($value) && is_string($value) && function_exists($value)) {
      return $value;
    } else if ($nullable) {
      return NULL;
    } else {
      return '';
    }
  }

}
