<?php

namespace Drupal\mixin;

use Drupal\mixin\Traits\Object;
use Exception;

class Caller {
  use Object;
  const TYPE_CLOSURE = 'closure';
  const TYPE_FUNCTION = 'function';
  const TYPE_METHOD = 'method';
  const TYPE_NONE = '';

  /** @var callable */
  protected $callback;
  protected $default;
  /** @var callable */
  protected $exceptionHandler;

  protected function __construct($callback = NULL, $default = NULL, $exceptionHandler = NULL) {
    $this->setCallback($callback)->setDefault($default)->setExceptionHandler($exceptionHandler);
  }

  function setExceptionHandler($value = NULL) {
    $this->exceptionHandler = Filter::sanitizeCallback($value, TRUE);
    return $this;
  }

  function setDefault($value = NULL) {
    $this->default = $value;
    return $this;
  }

  function setCallback($value = NULL) {
    $this->callback = Filter::sanitizeCallback($value, TRUE);
    return $this;
  }

  static function getCallableName($callback, $syntax_only = FALSE) {
    $name = '';
    is_callable($callback, $syntax_only, $name);
    return $name;
  }

  static function getCallbackType($callback, $syntax_only = FALSE) {
    $name = '';

    if (FALSE == is_callable($callback, $syntax_only, $name)) {
      return static::TYPE_NONE;
    }

    if (is_string($callback) && function_exists($callback)) {
      return static::TYPE_FUNCTION;
    }

    list($class, $method) = explode('::', $name);

    if (method_exists($class, $method)) {
      return static::TYPE_METHOD;
    } else if (is_callable($callback) && ('Closure' == $class) && ('__invoke' == $method)) {
      return static::TYPE_CLOSURE;
    } else {
      return static::TYPE_NONE;
    }
  }

  static function invoke($callback, array $arguments = [], $default = NULL, $exceptionHandler = NULL) {
    $caller = static::create($callback, $default, $exceptionHandler);
    return call_user_func_array($caller, $arguments);
  }

  static function create($callback = NULL, $default = NULL, $exceptionHandler = NULL) {
    return new static($callback, $default, $exceptionHandler);
  }

  static function createFunction($function, $parameter, $body) {
    if (function_exists($function)) {
      return;
    }

    $eval = "namespace { function $function ($parameter) { $body } }";
    eval($eval);
  }

  static function underscoreFunctionName(array $names, $strict = TRUE) {
    return implode('_', Filter::sanitizeNames($names, $strict));
  }

  function __invoke() {
    $default = $this->default;

    try {
      $callback = $this->callback;
      return isset($callback) ? call_user_func_array($callback, func_get_args()) : $default;
    } catch (Exception $exception) {
      $exceptionHandler = $this->exceptionHandler;

      if (isset($exceptionHandler)) {
        call_user_func($exceptionHandler, $exception, $this);
      }

      return $default;
    }
  }

}
