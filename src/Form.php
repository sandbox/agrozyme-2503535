<?php

namespace Drupal\mixin;

use Drupal\mixin\Traits\Object;

abstract class Form {
  use Object;

  protected static $hook = '';
  protected $form = [];
  protected $state = [];
  protected $values = [];
  protected $arguments = [];
  protected $module = '';

  function __construct(&$form, &$form_state) {
    $this->form = &$form;
    $this->state = &$form_state;
    $this->values = $this->getValues();
    $this->arguments = $this->getArguments();
    $this->module = static::getType()->getModule();
  }

  protected function getValues() {
    $state = $this->state;
    form_state_values_clean($state);
    $items = $this->fetchArray($state, ['values']);
    return $items;
  }

  protected function fetchArray(&$items, array $indexes = []) {
    return Getter::create($items, $indexes)->setBuilder(Caller::create(NULL, []))->fetch(FALSE);
  }

  protected function getArguments() {
    $items = $this->fetchArray($this->state, ['build_info', 'args']);
    return $items;
  }

  static function hookForm(&$form, &$form_state) {
    $item = static::create($form, $form_state);
    return $item->prepare() ? $item->doForm() : $form;
  }

  /** @return self */
  static function create(&$form, &$form_state) {
    return new static($form, $form_state);
  }

  protected function prepare() {
    return TRUE;
  }

  protected function doForm() {
    $form = &$this->form;
    $attached = &$form['#attached'];
    $attached['library'] = $this->attachedLibrary();
    $attached['js'] = $this->attachedScript();
    $attached['css'] = $this->attachedStyle();
    return $form;
  }

  protected function attachedLibrary() {
    $items = $this->fetchArray($this->form, ['#attached', 'library']);
    return $items;
  }

  protected function attachedScript() {
    $items = $this->fetchArray($this->form, ['#attached', 'js']);
    $setting = $this->attachedSetting();

    if (FALSE == empty($setting)) {
      $item = ['data' => [$this->module => $setting], 'type' => 'setting'];
      array_unshift($items, $item);
    }

    return $items;
  }

  protected function attachedSetting() {
    return [];
  }

  protected function attachedStyle() {
    $items = $this->fetchArray($this->form, ['#attached', 'css']);
    return $items;
  }

  static function hookValidate(&$form, &$form_state) {
    $item = static::create($form, $form_state);
    return $item->doValidate();
  }

  protected function doValidate() {
    return static::validateValues($this->values);
  }

  static function validateValues(array $values) {
    return TRUE;
  }

  static function hookSubmit(&$form, &$form_state) {
    return static::create($form, $form_state)->doSubmit();
  }

  protected function doSubmit() {
    return [];
  }

  static function createHookForm() {
    $class = get_called_class();
    $names = [static::getType()->getModule(), static::$hook, 'form'];
    $form = Caller::underscoreFunctionName($names, FALSE);
    $argument = '$form, $form_state';
    $items = [
      'hookForm' => $form,
      'hookValidate' => $form . '_validate',
      'hookSubmit' => $form . '_submit',
    ];

    foreach ($items as $method => $function) {
      Caller::createFunction($function, '$form, &$form_state', "return {$class}::{$method}({$argument});");
    }
  }

  static function createHookFormAlter($form_id = '') {
    $class = get_called_class();
    $names = [static::getType()->getModule(), 'form', $form_id, 'alter'];
    $function = Caller::underscoreFunctionName($names, FALSE);
    $argument = '$form, $form_state';
    Caller::createFunction($function, '&$form, &$form_state', "return {$class}::hookForm({$argument});");
  }

}
