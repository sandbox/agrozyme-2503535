<?php

namespace Drupal\mixin;

use Drupal\mixin\Traits\Object;

class Getter {
  use Object;

  protected $reference;
  protected $allowEmpty = TRUE;
  /** @var Caller */
  protected $builder;

  protected function __construct(&$items, array $indexes = []) {
    $data = &$items;

    foreach ($indexes as $index) {
      if (isset($data) && is_object($data)) {
        $data = &$data->$index;
      } else {
        $data = &$data[$index];
      }
    }

    $this->reference = &$data;
  }

  static function create(&$items, array $indexes = []) {
    return new static($items, $indexes);
  }

  function setBuilder($value = NULL) {
    $this->builder = static::callerWrapper($value);
    return $this;
  }

  protected function callerWrapper($callback) {
    if (is_object($callback) && $callback instanceof Caller) {
      return $callback;
    } else if (is_bool($callback)) {
      return Caller::create([], (bool)$callback);
    } else {
      return Caller::create($callback);
    }
  }

  function &reference() {
    return $this->reference;
  }

  function fetch($writeBack = NULL) {
    $reference = &$this->reference;

    if ($this->needBuilding()) {
      $builder = $this->builder;
      $item = isset($builder) ? $builder() : NULL;

      if (FALSE == $this->canWriteBack($item, $writeBack)) {
        return $item;
      }

      $reference = $item;
    }

    return $reference;
  }

  protected function needBuilding() {
    $reference = &$this->reference;
    return ($this->allowEmpty) ? (FALSE == isset($reference)) : empty($reference);
  }

  protected function canWriteBack($item, $callback = NULL) {
    return isset($callback) ? static::callerWrapper($callback)->__invoke($item) : TRUE;
  }

  function setAllowEmpty($value) {
    $this->allowEmpty = (bool)$value;
    return $this;
  }

}
