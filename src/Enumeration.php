<?php
namespace Drupal\mixin;

use Drupal\mixin\Traits\PropertyBagRegistry;

abstract class Enumeration {
  use PropertyBagRegistry {
    __construct as protected;
    getItem as protected;
  }

  protected $value;

  /** @return self|null */
  protected static function getObject($index, $value) {
    static $cache = [];

    $builder = function () use ($value) {
      return new static(compact('value'));
    };

    $item = Getter::create($cache, [get_called_class(), $index])->setBuilder($builder)->fetch();

    return $item;
  }

  protected static function setupMethodPattern() {
    return [];
  }

  function getValue() {
    return $this->value;
  }

}
