<?php

namespace Drupal\mixin;

use Drupal\mixin\Decimal\Base;
use Drupal\mixin\Decimal\BinaryCalculator;
use Drupal\mixin\Traits\Object;

class Decimal {
  use Object;

  /** @return Base */
  static function create($value, $scale = NULL) {
    static $cache;

    $builder = function () {
      return extension_loaded('BCMath') ? BinaryCalculator::className() : '';
      //if (extension_loaded('gmp')) {
      //  return MultiplePrecision::className();
      //} else if (extension_loaded('BCMath')) {
      //  return BinaryCalculator::className();
      //} else {
      //  return '';
      //}
    };

    $class = Getter::create($cache)->setBuilder($builder)->fetch();
    return call_user_func([$class, __FUNCTION__], $value, $scale);
  }

}
