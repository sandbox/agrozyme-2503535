<?php
namespace Drupal\mixin;

use Drupal\mixin\Traits\Object;
use stdClass;
use Traversable;

class Arrays {
  use Object;

  static function walk($items, $callback) {
    if (FALSE == static::isDispatchable($items, $callback)) {
      return;
    }

    foreach ($items as $index => $item) {
      $arguments = func_get_args();
      $arguments[0] = $index;
      $arguments[1] = $item;
      call_user_func_array($callback, $arguments);
    }
  }

  static function isDispatchable($items, $callback) {
    return is_callable($callback) && static::isTraversable($items);
  }

  static function isTraversable($items) {
    return (is_array($items) || ($items instanceof Traversable));
  }

  static function filter($items, $callback) {
    if (FALSE == static::isDispatchable($items, $callback)) {
      return [];
    }

    $data = [];

    foreach ($items as $index => $item) {
      $arguments = func_get_args();
      $arguments[0] = $index;
      $arguments[1] = $item;

      if (call_user_func_array($callback, $arguments)) {
        $data[$index] = $item;
      }
    }

    return $data;
  }

  static function map($items, $callback) {
    if (FALSE == static::isDispatchable($items, $callback)) {
      return $items;
    }

    $data = [];

    foreach ($items as $index => $item) {
      $arguments = func_get_args();
      $arguments[0] = $index;
      $arguments[1] = $item;
      $data[$index] = call_user_func_array($callback, $arguments);
    }

    return $data;
  }

  static function toObject(array &$items, $recursive = FALSE) {
    $object = new stdClass();

    if ($recursive) {
      return json_decode(json_encode($items), FALSE);
    }

    foreach ($items as $index => &$item) {
      $object->$index = &$item;
    }

    return $object;
  }
}
