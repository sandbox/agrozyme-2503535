<?php

namespace Drupal\mixin;

abstract class Schema extends SettingExport {

  static function hookSchema() {
    return static::export();
  }

  static function createHookSchema() {
    $class = get_called_class();
    $names = [static::getType()->getModule(), 'schema'];
    $function = Caller::underscoreFunctionName($names, FALSE);
    Caller::createFunction($function, '', "return {$class}::hookSchema();");
  }

  protected static function exportMethodPattern() {
    return '/^table[A-Z]/';
  }
}
