<?php
namespace Drupal\mixin\Parameter;

use Drupal\mixin\Caller;
use Drupal\mixin\Traits\Object;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionMethod;
use ReflectionParameter;

class ParserList {
  use Object;

  /** @var Parser[] */
  protected $items = [];

  protected function __construct(ReflectionFunctionAbstract $reflector = NULL) {
    if (isset($reflector)) {
      $mapping = function (ReflectionParameter $item) {
        return Parser::create($item);
      };

      $this->items = array_map($mapping, $reflector->getParameters());
    }
  }

  static function create($callback) {
    if ($callback instanceof ReflectionFunctionAbstract) {
      return new static($callback);
    }

    switch (Caller::getCallbackType($callback, TRUE)) {
      case Caller::TYPE_FUNCTION:
      case Caller::TYPE_CLOSURE:
        $reflector = new ReflectionFunction($callback);
      break;
      case Caller::TYPE_METHOD:
        list($class, $method) = explode('::', Caller::getCallableName($callback, TRUE));
        $reflector = new ReflectionMethod($class, $method);
      break;
      default:
        $reflector = NULL;
    }

    return new static($reflector);
  }

  function getItems() {
    return $this->items;
  }

  function toParameterListString() {
    return implode(',', $this->toParameters());
  }

  function toParameters() {
    $callback = function (Parser $item) {
      return $item->parameter();
    };

    return array_map($callback, $this->items);
  }

  function toArgumentListString() {
    return implode(',', $this->toArguments());
  }

  function toArguments() {
    $callback = function (Parser $item) {
      return $item->argument();
    };

    return array_map($callback, $this->items);
  }

}
