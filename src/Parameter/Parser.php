<?php

namespace Drupal\mixin\Parameter;

use Drupal\mixin\Traits\Object;
use Exception;
use ReflectionParameter;

class Parser {
  use Object;

  /** @var ReflectionParameter */
  protected $reflector;
  protected $prefix = '';
  protected $name = '';
  protected $suffix = '';

  protected function __construct(ReflectionParameter $reflector) {
    $this->prefix = ($reflector->isPassedByReference()) ? '&' : '';
    $this->name = '$' . $reflector->name;
    $suffix = &$this->suffix;

    if (static::isDefaultValueConstant($reflector)) {
      $suffix = '=' . $reflector->getDefaultValueConstantName();
    } elseif (static::isDefaultValueAvailable($reflector)) {
      $suffix = '=' . var_export($reflector->getDefaultValue(), TRUE);
    } else {
      $suffix = '';
    }
  }

  static function isDefaultValueConstant(ReflectionParameter $reflector) {
    try {
      return $reflector->isDefaultValueConstant();
    } catch (Exception $exception) {
      return FALSE;
    }
  }

  static function isDefaultValueAvailable(ReflectionParameter $reflector) {
    try {
      return $reflector->isDefaultValueAvailable();
    } catch (Exception $exception) {
      return FALSE;
    }
  }

  static function create(ReflectionParameter $reflector) {
    return new static($reflector);
  }

  function getReflector() {
    return $this->reflector;
  }

  function parameter() {
    return $this->prefix . $this->name . $this->suffix;
  }

  function argument() {
    return $this->name;
  }

  function getName() {
    return $this->name;
  }

  function getPrefix() {
    return $this->prefix;
  }

  function getSuffix() {
    return $this->suffix;
  }
}
